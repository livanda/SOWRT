call plug#begin('~/.vim/plugged')
"
"Plug 'Valloric/YouCompleteMe'
" Auto Pairs закрывает скобки 
Plug 'jiangmiao/auto-pairs'
" Валидатор 
Plug 'maralla/validator.vim'

" HTML
"
 html 5 syntax
"Plug 'othree/html5.vim'
" Емит 
Plug 'mattn/emmet-vim'
" поцветка синтексита HTML5
"Plug 'othree/html5.vim'
" Angular 2 snippets
Plug 'mhartington/vim-angular2-snippets'
" Anguar syntax
Plug 'curist/vim-angular-template'
 jade syntax
"Plug 'digitaltoad/vim-jade'
" SVG syntax
Plug 'vim-scripts/svg.vim'
"
" CSS
"
" Подсветка цветов CSS
Plug 'ap/vim-css-color'
" css syntax
Plug 'JulesWang/css.vim'
" scss syntax
Plug 'cakebaker/scss-syntax.vim'

"
" JS
"
" json syntax
Plug 'elzr/vim-json'
" typescript syntax
"Plug 'leafgarland/typescript-vim'
" JQuery syntax
"Plug 'nono/jquery.vim'
" Vue JS syntax
"Plug 'posva/vim-vue'
" JS syntax
"Plug 'pangloss/vim-javascript'
" PHP
"
" php syntax
"Plug 'stanangeloff/php.vim'

" 
" Навигация 
"
" Дерево католога 
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
" Посик по всем каталогам ctrl+p
"Plug 'kien/ctrlp.vim'

"
" Работа с git 
"
" Вызов команд гита через вим 
"Plug 'tpope/vim-fugitive'
" чек боксы из гита 
"Plug 'airblade/vim-gitgutter'

" Unix
"

" Bash syntax
"Plug 'vim-scripts/bats.vim'


"" Темы
"
" Цветовая тема
"Plug 'morhetz/gruvbox'

" Закрытие пакетного менаджерва 
call plug#end()
"
"--------------------------------
"
" js sintax
"Enables syntax highlighting for JSDocs.
"let g:javascript_plugin_jsdoc = 1
"let g:javascript_plugin_flow = 1
"let g:javascript_plugin_ngdoc = 1
"set foldmethod=syntax

"
" Настройки
"
" Номирация сторк 
set number
" отступы tab 
set tabstop=2
set shiftwidth=2
" Подцветка поиска 
set hlsearch 
" Инруминтальная подцветка поиска
set incsearch
" Подцветка кода 
syntax on 
" morhetz/gruvbox
"colorscheme gruvbox
"set background=dark


"--------------------------------


" ###################
" Горячие Клавиши
" ###################
" Сохранить
imap	<F2>	<Esc>:w!<CR>
nmap	<F2>	<Esc>:w!<CR>
" Выход Без Сохраниения 
imap	<F3>	<Esc>:q!<CR>
nmap	<F3>	<Esc>:q!<CR>
" Сохрнить и выйти
imap	<F4>	 <Esc>:wq!<CR>
nmap	<F4>	<Esc>:wq!<CR>

" Откртытие NERDTree на сочтитение ctr+n
map <C-n> :NERDTreeToggle<CR>
" Перечитаь ~/.vimrc
map <F5> :source ~/.vimrc<CR>
" Перечитаь ~/.vimrc и запустить установку плагинов 
map <F6> :source ~/.vimrc<CR>:PlugInstall<CR>
" Обновить пактеы 
map <F7> :PlugUpdate<CR>


" Открытие эмита на ТАБ+,
let g:user_emmet_leader_key='<Tab>'
let g:emmet_stacked_multiplication=0

" ###################
" Навигация по окнам 
" ###################
" Открыть оконо в лево
map <silent> <C-h> :call WinMove('h')<CR>
" Открыть оконо в вниз
map <silent> <C-j> :call WinMove('j')<CR>
" Открыть оконо в вверх
map <silent> <C-k> :call WinMove('k')<CR>
" Открыть оконо в лево
map <silent> <C-l> :call WinMove('l')<CR>

function! WinMove(key)
	let t:curwin =winnr()
	exec "wincmd ".a:key
	if (t:curwin == winnr())
					if (match(a:key,'[jk]'))
									wincmd v
							else
									wincmd s
							endif
							exec "wincmd ".a:key
				endif
endfunction

" Комамнды на кирилице 
map ё `
map й q
map ц w
map у e
map к r
map е t
map н y
map г u
map ш i
map щ o
map з p
map х [
map ъ ]
map ф a
map ы s
map в d
map а f
map п g
map р h
map о j
map л k
map д l
map ж ;
map э '
map я z
map ч x
map с c
map м v
map и b
map т n
map ь m
map б ,
map ю .
map Ё ~
map Й Q
map Ц W
map У E
map К R
map Е T
map Н Y
map Г U
map Ш I
map Щ O
map З P
map Х {
map Ъ }
map Ф A
map Ы S
map В D
map А F
map П G
map Р H
map О J
map Л K
map Д L
map Ж :
map Э "
map Я Z
map Ч X
map С C
map М V
map И B
map Т N
map Ь M
map Б <
map Ю >

"--------------------------------

" ###################
" включить сохранение резервных копий
" ###################
set backup
" сохранять умные резервные копии ежедневно
function! BackupDir()
	" определим каталог для сохранения резервной копии
	let l:backupdir=$HOME.'/.vim/backup/'.
			\substitute(expand('%:p:h'), '^'.$HOME, '~', '')
	" если каталог не существует, создадим его рекурсивно
	if !isdirectory(l:backupdir)
		call mkdir(l:backupdir, 'p', 0700)
	endif
	" переопределим каталог для резервных копий
	let &backupdir=l:backupdir
	" переопределим расширение файла резервной копии
	let &backupext=strftime('~%Y-%m-%d~')
endfunction
" выполним перед записью буффера на диск
autocmd! bufwritepre * call BackupDir()

"#######################
" Насторойки .vimrc от
"     2017.02.01
"#######################

